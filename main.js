console.log("Welcome to jQuery.");



// Botones efecto

$(document).ready(function(){

  $(".boton").hover(function(){
    $(".boton").addClass('hoverbut')
    $(this).removeClass('hoverbut');
    $(this).css("color", "white")
  }, function() {
    $(".boton").removeClass('hoverbut');
  });

// Contactanos scroll

  $(document).ready(function(){
    $("#scrol").on('click', function(event) {

      if (this.hash !== "") {
        event.preventDefault();

        var hash = this.hash;

        $('html, body').animate({
          scrollTop: $(hash).offset().top
        }, 800, function(){

          window.location.hash = hash;
        });
      }
    });
  });



});
